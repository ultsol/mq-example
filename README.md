To get started:

```
go get github.com/nats-io/gnatsd
gnatsd &
npm i

node mq-worker.js
node mq-worker.js
node mq-worker.js # (in different consoles)

node mq-client.js # (several times)
```

You will see that cleint's requests are served by different workers.
So that the workload is authomatically balanced.

