const NATS = require('nats');
let nats = NATS.connect();

let r = {id: 'foo_bar_id', url:'http://ya.ru', timeout: 10};

nats.publish('scrap', JSON.stringify(r));

nats.subscribe('scrap-result', (reply) => {
  console.log('scrap result:', reply);
  nats.close();
  console.log('end');
});

// vim: set ts=2 sw=2 sts=2 et:

