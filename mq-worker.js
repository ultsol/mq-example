const shortid = require('shortid');
const NATS = require('nats');
let nats = NATS.connect();

async function scrap(url, timeout) {
  let LOG = console.log;

  LOG("url:", url);
  LOG("timeout:", timeout);
  //TODO: scrap here!
  return 'scraped result!';
}

let worker_id = shortid.generate();

nats.subscribe('scrap', {queue: 'q'}, function(req) {
  (async() => {
    let r = JSON.parse(req);
    let contents = await scrap(r.url, r.timeout);
    nats.publish('scrap-result', contents + '; worker_id=' + worker_id);
  })();
});

// vim: set ts=2 sw=2 sts=2 et:

